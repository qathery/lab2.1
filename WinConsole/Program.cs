﻿using System;

namespace WinConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double x = Convert.ToDouble(Console.ReadLine());
                double y = Convert.ToDouble(Console.ReadLine());
                double z = Convert.ToDouble(Console.ReadLine());
                double s;
                s = (2 * Math.Cos(Math.Pow(x, 2)) - 0.5) / (0.5 + Math.Sin(Math.Pow(y, 2 - z))) + (Math.Pow(z, 2) / (7 - z / 3));


                Console.WriteLine(s.ToString("F" + 2));
            }
            catch
            {
                Console.WriteLine("error");
                Console.ReadKey();
            }
            
        }
    }
}
