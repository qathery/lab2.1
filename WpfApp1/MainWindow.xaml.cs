﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double x = Convert.ToDouble(a.Text);
                double y = Convert.ToDouble(b.Text);
                double z = Convert.ToDouble(c.Text);
                double s;
                s = (2 * Math.Cos(Math.Pow(x, 2)) - 0.5) / (0.5 + Math.Sin(Math.Pow(y, 2 - z))) + (Math.Pow(z, 2) / (7 - z / 3));

                l.Text = s.ToString("F2");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка введення значення z!", "Помилка");
                a.Text = "";
                a.Clear();
                b.Text = "";
                b.Clear();
                c.Text = "";
                c.Clear();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
